Do TrainingInstanceFacade by som implementoval funkciu getAllUserScoreFromTrainingInstance(Long trainingInstanceId, bool isActive, Pageable pageable)
ktorá by vrátila zoznam DTO, ktoré by držalo dvojicu <userId, score>. Toto DTO by bolo tiež nutné implementovať.
Táto funkcia by najskôr získala všetky TrainingRun-y z TrainingInstanceService pomocou id z argumentu.
Z toho by som potom získal userId aj score a vytvoril zoznam DTO. V prípade že netreba riešiť či je TrainingRun aktívny alebo nie
tak argument isActive by som odstránil.

Do TrainingInstancesRestController by som pridal funkciu getAllUserScoreFromTrainingInstance(Long trainingInstanceId) ktorá by zabalila výsledok novej funkcie
do ResponseEntity. Potom by len bolo nutné doplniť anotácie a popisy podľa požiadavkov.


