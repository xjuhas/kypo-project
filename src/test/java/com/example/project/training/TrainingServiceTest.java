package com.example.project.training;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TrainingServiceTest {

    @Mock
    private TrainingRepository trainingRepository;
    private TrainingService trainingService;

    @BeforeEach
    void init() {
        trainingService = new TrainingService(trainingRepository);
    }

    @Test
    void getCSVDataTest() {
        trainingService.getCSVData(1L, ';');
        verify(trainingRepository).findByTrainingId(1L);
    }

    @Test
    void getAllCSVDataTest() {
        trainingService.getAllCSVData(';');
        verify(trainingRepository).findAll();
    }

}