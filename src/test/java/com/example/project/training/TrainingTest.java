package com.example.project.training;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TrainingTest {

    @Test
    void csvString() {
        Training training = new Training(47987L, 45L, 1);
        Character delimiter = ';';

        assertEquals("47987;45;1", training.csvString(delimiter));

        training = new Training(7987891L, 1233124L, 4677);
        assertEquals("7987891;1233124;4677", training.csvString(delimiter));
    }
}