package com.example.project.training;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class TrainingRepositoryTest {

    @Autowired
    private TrainingRepository trainingRepository;

    @Test
    void findByTrainingIdTest() {
        Long trainingId = 84L;
        Training training = new Training(trainingId, 15L, 78);
        trainingRepository.save(training);

        List<Training> trainings = trainingRepository.findByTrainingId(trainingId);

        assertEquals(1, trainings.size());
        assertEquals(training, trainings.get(0));
    }

    @Test
    void findByTrainingIdNoTrainingFoundTest() {
        Long trainingId = 464L;
        assertEquals(0, trainingRepository.findByTrainingId(trainingId).size());
    }

    @Test
    void findByTrainingIdMultipleTrainings() {
        Long trainingId = 580L;
        List<Training> newTrainings = List.of(
                new Training(trainingId, 77L, 1),
                new Training(trainingId, 78L, 5),
                new Training(trainingId, 79L, 789)
        );

        trainingRepository.saveAll(newTrainings);

        List<Training> trainings = trainingRepository.findByTrainingId(trainingId);
        assertEquals(3, trainings.size());

        for (int i = 0; i < 3; i++) {
            assertEquals(newTrainings.get(i), trainings.get(i));
        }
    }
}