package com.example.project.training;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrainingRepository extends JpaRepository<Training, CompKey> {
    List<Training> findByTrainingId(Long trainingId);
}
