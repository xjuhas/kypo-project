package com.example.project.training;

import java.io.Serializable;
import java.util.Objects;

public class CompKey implements Serializable {
    private Long trainingId;
    private Long studentId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompKey compKey = (CompKey) o;
        return trainingId.equals(compKey.trainingId) && studentId.equals(compKey.studentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainingId, studentId);
    }
}
