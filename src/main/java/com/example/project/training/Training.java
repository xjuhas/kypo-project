package com.example.project.training;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name="training")
@IdClass(CompKey.class)
public class Training {
    @Id
    private Long trainingId;
    @Id
    private Long studentId;
    private Integer score;

    public Training() {}

    public Training(Long trainingId, Long studentId, Integer score) {
        this.trainingId = trainingId;
        this.studentId = studentId;
        this.score = score;
    }

    public Long getTrainingId() {
        return trainingId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String csvString(Character delimiter) {
        return String.valueOf(trainingId) + delimiter + studentId + delimiter + score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Training training = (Training) o;
        return trainingId.equals(training.trainingId) && studentId.equals(training.studentId) && score.equals(training.score);
    }

    @Override
    public int hashCode() {
        return Objects.hash(trainingId, studentId, score);
    }
}
