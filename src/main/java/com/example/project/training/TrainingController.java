package com.example.project.training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrainingController {
    @Autowired
    private TrainingService service;
    private final Character DELIMITER = ';';

    @GetMapping(value = "/data", produces = "text/csv")
    public String getTrainingData() {
        return service.getAllCSVData(DELIMITER);
    }

    @GetMapping(value = "/data/{trainingId}", produces = "text/csv")
    public String getTrainingData(@PathVariable long trainingId) {
        return service.getCSVData(trainingId, DELIMITER);
    }
}
