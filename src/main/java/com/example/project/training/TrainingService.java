package com.example.project.training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TrainingService {
    @Autowired
    private TrainingRepository repository;

    public TrainingService(TrainingRepository trainingRepository) {
        repository = trainingRepository;
    }

    /**
     * Creates and returns a CSV string from the data provided using the delimiter
     * @param data data to use
     * @param delimiter delimiter to use
     * @return CSV string from the data provided
     */
    private String makeCSV(List<Training> data, Character delimiter) {
        StringBuilder result = new StringBuilder();
        for (Training row : data) {
            result.append(row.csvString(delimiter)).append(System.lineSeparator());
        }
        return result.toString();
    }

    /**
     * Returns all training data from a specific training in a CSV string using the delimiter provided
     * @param trainingId id of the training
     * @param delimiter delimiter to use
     * @return CSV string of the training data
     */
    public String getCSVData(long trainingId, Character delimiter) {
        return makeCSV(repository.findByTrainingId(trainingId), delimiter);
    }

    /**
     * Returns all training data in a CSV string using the delimiter provided
     * @param delimiter delimiter to use
     * @return CSV string of the training data
     */
    public String getAllCSVData(Character delimiter) {
        return makeCSV(repository.findAll(), delimiter);
    }
}
